import {Component} from '@angular/core'


@Component({
    selector: 'events-list',
    templateUrl: './events-list.component.html',
    styles: [`
    .likes div {color: red;}
    `
        
    ]
})

export class EventsListComponent{

    eventData1 = {
        id: 1,
        name: 'Angular Connect',
        date: '9/26/2019',
        time: '10:00am',
        price: '100',
        imageUrl: '/assets/images/basic-shield.png',
        location: {
            address: 'sdaf',
            city: 'safsd',
            country: 'aaa'
        }
    }

    handleFunction(param: string): void {
        console.log(param)
    }
    likes: number;
}