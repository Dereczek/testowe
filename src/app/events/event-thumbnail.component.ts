import { Component, Input, Output, EventEmitter } from '@angular/core';



@Component({

    selector: 'event-thumbnail',
    template: `
<div class="well hoverwell thumbnail">
    <h2>{{event.name}}</h2>
    <div>Date: {{event.date}}</div>
    <div>Time: {{event.time}}</div>
    <div>Price: \${{event.price}}</div>
    <button (click)="clickMeFn()">Click me</button>
</div>
    `

})

export class EventThumbnailComponent{
   likes: number = 10;

   @Input() event: any;
   @Output() eventClick = new EventEmitter();

   clickMeFn(): void {
       this.eventClick.emit('foo');
   }




}